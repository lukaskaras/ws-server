// @ts-ignore
import express from 'express'
const app = express()
import bodyParser from 'body-parser'
const socketio = require('socket.io')
import * as http from 'http'
const server = http.createServer(app)

server.listen(5001)
const io = socketio(server)

io.on('connection', (socket) => {
  console.log('connection created')
})

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.get('/bla', (req, res) => {
  io.emit('message', 'important message')
  res.sendStatus(200)
})

app.post('/video', (req, res, err) => {
  console.log('req.body', req.body)
  const { media, type } = req.body
  console.log('media', media)
  io.emit('media', JSON.stringify({ type, media }))
  res.sendStatus(200)
})

app.listen(5000, () => {
  console.log('listening')
})
